package com.amit.util;

import static org.junit.jupiter.api.Assertions.*;
import  org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

class UtilitiesTest {

    private List<String> listTest = null;

    @BeforeEach
    private void getData(){
         listTest = new ArrayList<>();
         listTest.add("Rakesh");
         listTest.add("Amit");
    }

    @Test
    void listToString() {
        assertTrue((Utilities.listToString(listTest, ",").equals("Rakesh,Amit")));
    }

}