package com.amit.util;

import java.util.List;

public class Utilities {

    public static String listToString(List<String> stringList, String sep){
        String result = null;
        int index = 0;
        for(String list : stringList){
            if(result == null){
                result = list;
            } else {
                index ++;
                result = result + sep + list;
            }
        }

        return result;
    }

}